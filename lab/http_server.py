# -*- encoding: utf-8 -*-

import socket


def http_serve(server_socket, html):
    while True:
        # Czekanie na połączenie
        connection, client_address = server_socket.accept()

        try:
            # Odebranie żądania
            request = connection.recv(1024)
            if request:
                str = request.split("\n")
                if "HTTP" in str[0] and "GET" in str[0]:
                    print(str[0])
                    print "Odebrano:"
                else:
                    print("nie ma")
                # Wysłanie zawartości strony
                connection.sendall(html)

        finally:
            # Zamknięcie połączenia
            connection.close()


# Tworzenie gniazda TCP/IP
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Ustawienie ponownego użycia tego samego gniazda
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# Powiązanie gniazda z adresem
server_address = ('localhost', 4005)  # TODO: zmienić port!
server_socket.bind(server_address)

# Nasłuchiwanie przychodzących połączeń
server_socket.listen(1)

html = open('web/web_page.html').read()

try:
    http_serve(server_socket, html)

finally:
    server_socket.close()

