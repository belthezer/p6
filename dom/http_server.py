# -*- encoding: utf-8 -*-

import socket
import os

def http_serve(server_socket, html):
    while True:
        # Czekanie na połączenie
        connection, client_address = server_socket.accept()

        try:
            # Odebranie żądania
            request = connection.recv(1024)
            if request:
                str = request.split("\n")
                odpowiedz = ""
                print(request)
                if "HTTP" in str[0] and "GET" in str[0]:
                    adres = str[0]
                    if "Host" in str[1]: host = str[1]
                    if "Host" in str[2]: host = str[2]
                    host = host[6:]
                    adres = adres[5:-10]
                    if adres == "": adres = "."
                    #print adres
                    try:
                        dirList = os.listdir(adres)
                        parentDir = adres.split("/")
                        if len(parentDir)>1:
                            parentDir.pop(len(parentDir)-1)
                            parentDir.pop(len(parentDir)-1)
                        parentDirPath = ""
                        for pth in parentDir:
                            parentDirPath+="/"+pth
                        odpowiedz+="<h1>Serwer</h1>"
                        odpowiedz+="<a href=\""+parentDirPath+"/\">...</a><br />"
                        for cos in dirList:
                            if "." in cos:
                            #if cos[-4:] == ".png" or cos[-4:] == ".jpg" or cos[-4:] == ".txt" or cos[-3:] == ".py" or cos[-4:] == ".rst" or cos[-5:] == ".html":
                                odpowiedz+="<a href=\""+""+""+cos+"\">"+cos+"</a><br />"
                            else:
                                odpowiedz+="<a href=\""+""+""+cos+"/\">"+cos+"</a><br />"
                        if adres[-1:] != "/" and adres != ".": odpowiedz = "<html><head><meta HTTP-EQUIV='REFRESH' content='0; url=http://"+host+"/"+adres+"/'></head><body></body></html>"
                        #odpowiedz = open('web/images/jpg_rip.jpg').read()
                        odpowiedz = "HTTP/1.1 200 OK Content-Type: text/html; charset=UTF-8\r\n\r\n"+odpowiedz
                    except:
                        try:
                            odpowiedz = open(adres, "rb").read()
                            if adres[-4:] == ".jpg":
                                odpowiedz = "HTTP/1.1 200 OK Content-Type: image/jpeg; charset=UTF-8\r\n\r\n"+odpowiedz
                            if adres[-4:] == ".png":
                                odpowiedz = "HTTP/1.1 200 OK Content-Type: image/png; charset=UTF-8\r\n\r\n"+odpowiedz
                            if adres[-5:] == ".html":
                                odpowiedz = "HTTP/1.1 200 OK Content-Type: text/html; charset=UTF-8\r\n\r\n"+odpowiedz
                            if adres[-4:] == ".txt":
                                odpowiedz = "HTTP/1.1 200 OK Content-Type: text/span; charset=UTF-8\r\n\r\n"+odpowiedz
                        except:
                            odpowiedz = "<h1>404 not found</h1>"+host+"/"+adres
                            odpowiedz = "HTTP/1.0 404 Not Found Content-Type: text/html; charset=UTF-8\r\n\r\n"+odpowiedz
                    #print "Odebrano:"
                else:
                    print("nie ma")
                # Wysłanie zawartości strony
                connection.sendall(odpowiedz)

        finally:
            # Zamknięcie połączenia
            connection.close()


# Tworzenie gniazda TCP/IP
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Ustawienie ponownego użycia tego samego gniazda
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# Powiązanie gniazda z adresem
server_address = ('localhost', 4005)  # TODO: zmienić port!
server_socket.bind(server_address)

# Nasłuchiwanie przychodzących połączeń
server_socket.listen(1)

html = ""#open('web/web_page.html').read()

try:
    http_serve(server_socket, html)

finally:
    server_socket.close()

